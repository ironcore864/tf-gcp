# Initial Setup for Terraform 0.12 with GCP GCS as Remote State Storage

## 1 TF Download and Install

https://www.terraform.io/downloads.html

## 2 GCP Preparation

Pre-requisites:

- GCP Project
- Google Compute Engine in the project

### 2.1 GCP Service Account

Create GCP service account key: In IAM, create service account with "Project -> Editor" role. Ask admin to do so if you haven't got permission.

Then create a key for the service account, choose JSON format and download.

### 2.2 GCS

In GCP - Storage, create a bucket, used for storing TF remote state.

## 3 GCP Provider

Check latest version from: https://registry.terraform.io/providers/hashicorp/google

config.tf example:

```
provider "google" {
  version = "3.12.0"

  credentials = file("the-json-key-file-from-your-service-account.json")

  project = "skyway-public"
  region  = "us-central1"
  zone    = "us-central1-a"
}

terraform {
  backend "gcs" {
    bucket      = "skyway-public-tf-state-test"
    prefix      = "terraform/state"
    credentials = "the-json-key-file-from-your-service-account.json"
  }
}

```

## 4 Directory Structure

```
.
├── modules
├── prod
└── test
    ├── config.tf
    ├── network.tf
    └── skyway-public-536d2eff1c9d.json
```

The json key should be stored securely, NOT commited to git (already in gitignore).

- modules: for terraform modules
- prod: for prod env
- test: for test env

If other ENVs are needed, create accordingly.

# git-crypt

## Install

On Mac OS: `brew install git-crypt`

On other platforms, see: https://github.com/AGWA/git-crypt/blob/master/INSTALL.md

## Initialize (already done in this repo, only for future reference)

See [official doc](https://github.com/AGWA/git-crypt).

## Generate GPG Key

Example:

You can use all default settings, and you must remember the password for the key.

```bash
tiexin@Tiexins-MacBook-Pro ~ $ gpg --full-generate-key
gpg (GnuPG) 2.2.17; Copyright (C) 2019 Free Software Foundation, Inc.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

Please select what kind of key you want:
   (1) RSA and RSA (default)
   (2) DSA and Elgamal
   (3) DSA (sign only)
   (4) RSA (sign only)
Your selection?
RSA keys may be between 1024 and 4096 bits long.
What keysize do you want? (2048)
Requested keysize is 2048 bits
Please specify how long the key should be valid.
         0 = key does not expire
      <n>  = key expires in n days
      <n>w = key expires in n weeks
      <n>m = key expires in n months
      <n>y = key expires in n years
Key is valid for? (0)
Key does not expire at all
Is this correct? (y/N) y

GnuPG needs to construct a user ID to identify your key.

Real name: Tiexin Guo
Email address: guotiexin@gmail.com
Comment:
You selected this USER-ID:
    "Tiexin Guo <guotiexin@gmail.com>"

Change (N)ame, (C)omment, (E)mail or (O)kay/(Q)uit? O
We need to generate a lot of random bytes. It is a good idea to perform
some other action (type on the keyboard, move the mouse, utilize the
disks) during the prime generation; this gives the random number
generator a better chance to gain enough entropy.
We need to generate a lot of random bytes. It is a good idea to perform
some other action (type on the keyboard, move the mouse, utilize the
disks) during the prime generation; this gives the random number
generator a better chance to gain enough entropy.
gpg: key 5F6E53188FC3D483 marked as ultimately trusted
gpg: revocation certificate stored as '/Users/tiexin/.gnupg/openpgp-revocs.d/0C83843F4EA9B184577BC4AB5F6E53188FC3D483.rev'
public and secret key created and signed.

pub   rsa2048 2020-03-13 [SC]
      0C83843F4EA9B184577BC4AB5F6E53188FC3D483
uid                      Tiexin Guo <guotiexin@gmail.com>
sub   rsa2048 2020-03-13 [E]
```

## Export the Key

First, run: `gpg -k` to list all keys. Example:

```
tiexin@Tiexins-MacBook-Pro ~/seirobotics/tf-gcp $ gpg -k
/Users/tiexin/.gnupg/pubring.kbx
--------------------------------
pub   rsa2048 2020-03-13 [SC]
      0C83843F4EA9B184577BC4AB5F6E53188FC3D483
uid           [ultimate] Tiexin Guo <guotiexin@gmail.com>
sub   rsa2048 2020-03-13 [E]
```

Copy the pub key which is a hexadecimal string, in this case it is "0C83843F4EA9B184577BC4AB5F6E53188FC3D483".

Export:

`gpg --armor --export --output user_pubkey.gpg 0C83843F4EA9B184577BC4AB5F6E53188FC3D483`

## Add Users into the Project

If your key hasn't been added into this repo, send your user_pubkey.gpg to the maintainer to add you.

If your pub key is already added, you can add other people's key, here's how:

First, import other people's key into your system:

`gpg --import user_pubkey.gpg`

Then go to the root dir of this repo, and add the key by running: `git-crypt add-gpg-user 0C83843F4EA9B184577BC4AB5F6E53188FC3D483`.

## En/Decrypt

After git clone, do `git-crypt unlock` to see the content of the file.

If it's edited, you can directly add and commit, it will be automatically encrypted.

If you don't want other people to see the content when you are not using the encrypted files, you can lock it up by `git-crypt lock`. Next time when you use it you can simply unlock it again.

# TF for GCP

## Initial Setup

See [initial_setup.md](initial_setup.md).

Version v0.12.23 required.

## Git-crypt

This repo uses git-crypt for sensitive data encryption.

Regarding how to use, see [git-crypt.md](git-crypt.md).

## Deploy

```
cd test
terraform init
rm -f tfplan
terraform plan -out tfplan
terraform apply tfplan
```

## Format

```
terraform fmt -recursive
```

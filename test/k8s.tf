module "gke" {
  source = "../modules/gke"

  naming_suffix = var.naming_suffix
}

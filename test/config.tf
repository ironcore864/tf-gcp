provider "google" {
  version = "3.12.0"

  credentials = file("skyway-public-536d2eff1c9d.json")

  project = "skyway-public"
  region  = "us-central1"
  zone    = "us-central1-a"
}

terraform {
  backend "gcs" {
    bucket      = "skyway-public-tf-state-test"
    prefix      = "terraform/state"
    credentials = "skyway-public-536d2eff1c9d.json"
  }
}
